#define AppVer "2.1.2.2"

[Dirs]
Name: "{app}\Component\falcon_43"
Name: "{app}\Component\falcon_442"
Name: "{app}\Component\falcon_444"
Name: "{app}\Component\ghost_422"
Name: "{app}\Component\ghost_44"
Name: "{app}\Component\ghost_442"
Name: "{app}\Driver\x64"
Name: "{app}\Driver\x86"
Name: "{app}\Release\Component"
Name: "{app}\Release\Component\falcon_43"
Name: "{app}\Release\Component\falcon_442"
Name: "{app}\Release\Component\falcon_444"
Name: "{app}\Release\Component\falcon_50"
Name: "{app}\Release\Component\ghost_422"
Name: "{app}\Release\Component\ghost_44"
Name: "{app}\Release\Component\ghost_442"
Name: "{app}\Release\Driver"
Name: "{app}\Release\Driver\x64"
Name: "{app}\Release\Driver\x86"
Name: "{app}\Release\Logs"
Name: "{app}\Release\Component"
Name: "{app}\Release\Component\falcon_43"
Name: "{app}\Release\Component\falcon_442"
Name: "{app}\Release\Component\falcon_444"
Name: "{app}\Release\Component\falcon_50"
Name: "{app}\Release\Component\ghost_422"
Name: "{app}\Release\Component\ghost_44"
Name: "{app}\Release\Component\ghost_442"
Name: "{app}\Release\Driver"
Name: "{app}\Release\Driver\x64"
Name: "{app}\Release\Driver\x86"
Name: "{app}\Release\Logs"
Name: "{app}\Component\falcon_43"
Name: "{app}\Component\falcon_442"
Name: "{app}\Component\falcon_444"
Name: "{app}\Component\falcon_50"
Name: "{app}\Component\ghost_422"
Name: "{app}\Component\ghost_44"
Name: "{app}\Component\ghost_442"
Name: "{app}\Driver\x64"
Name: "{app}\Driver\x86"
Name: "{app}\Component\falcon_43"
Name: "{app}\Component\falcon_442"
Name: "{app}\Component\falcon_444"
Name: "{app}\Component\falcon_50"
Name: "{app}\Component\ghost_422"
Name: "{app}\Component\ghost_44"
Name: "{app}\Component\ghost_442"

[Files]
Source: "{app}\MotoFlasher.conf"; DestDir: "{tmp}"; Flags: external skipifsourcedoesntexist; AfterInstall: UpdateCheck
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\FirstFloor.ModernUI.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Microsoft.Windows.Shell.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\MotoFlasher.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\clean.exe"; DestDir: "{app}\Driver"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x64\dpinst.exe"; DestDir: "{app}\Driver\x64"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x64\qcusbser.sys"; DestDir: "{app}\Driver\x64"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x64\Qualcomm_QDLoader_64bit_Driver.inf"; DestDir: "{app}\Driver\x64"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x86\dpinst.exe"; DestDir: "{app}\Driver\x86"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x86\qcusbser.sys"; DestDir: "{app}\Driver\x86"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Driver\x86\Qualcomm_QDLoader_32bit_Driver.inf"; DestDir: "{app}\Driver\x86"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\programmer.mbn"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\programmer_8626.mbn"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\qboot"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\qboot.dll"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\qboot.exe"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\qboot.ini"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\singleimage.bin"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_43\singleimage_8626.bin"; DestDir: "{app}\Component\falcon_43"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\programmer.mbn"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\programmer_8626.mbn"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\programmer_8926.mbn"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\qboot"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\qboot.dll"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\qboot.exe"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\qboot.ini"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\singleimage.bin"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\singleimage_8626.bin"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_442\singleimage_8926.bin"; DestDir: "{app}\Component\falcon_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\programmer.mbn"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\programmer_8626.mbn"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\programmer_8926.mbn"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\qboot"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\qboot.dll"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\qboot.exe"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\qboot.ini"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\singleimage.bin"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\singleimage_8626.bin"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_444\singleimage_8926.bin"; DestDir: "{app}\Component\falcon_444"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\falcon_50\placeholder"; DestDir: "{app}\Component\falcon_50"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\8960_blankflash.py"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\MPRG8960.hex"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\MSM8960_blankflash.xml"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\MSM8960_bootloader_singleimage.bin"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\qflash.dll"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_422\qflash.exe"; DestDir: "{app}\Component\ghost_422"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\8960_blankflash.py"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\MPRG8960.hex"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\MSM8960_blankflash.xml"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\MSM8960_bootloader_singleimage.bin"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\qflash.dll"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_44\qflash.exe"; DestDir: "{app}\Component\ghost_44"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\8960_blankflash.py"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\MPRG8960.hex"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\MSM8960_blankflash.xml"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\MSM8960_bootloader_singleimage.bin"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\qflash.dll"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "C:\Users\NahuelAlejandro\Documents\motoflasher\MotoFlasher\bin\Release\Component\ghost_442\qflash.exe"; DestDir: "{app}\Component\ghost_442"; Flags: ignoreversion
Source: "{tmp}\MotoFlasher.conf"; DestDir: "{app}"; Flags: external skipifsourcedoesntexist

[Setup]
AppName=Moto Flasher
AppCopyright=Negan1911 Co
AppId={{CBC9CDF8-C292-41DC-AA6C-C1D06CC81ED8}
AppPublisher=Negan1911
AppPublisherURL=http://www.negan1911.com
AppSupportURL=https://bitbucket.org/negan1911/motoflasher/issues
AppUpdatesURL=http://www.negan1911.com/motoflasher.php
RestartApplications=False
UninstallDisplayName=Negan1911
UninstallDisplayIcon={app}\MotoFlasher.exe
VersionInfoCompany=Negan1911
VersionInfoCopyright=Negan1911 Co
VersionInfoProductName=Moto Flasher
SolidCompression=True
DefaultDirName={pf}\MotoFlasher
LicenseFile=userdocs:motoflasher\MotoFlasher_Installer\installerReadme.txt
SetupIconFile=userdocs:motoflasher\MotoFlasher_Installer\MotoFlasher.ico
DefaultGroupName=Moto Flasher
WizardImageFile=userdocs:motoflasher\MotoFlasher_Installer\VerticalLogo.bmp
WizardSmallImageFile=userdocs:motoflasher\MotoFlasher_Installer\MiniLogo.bmp
VersionInfoDescription=MotoFlasher Allows you to flash Bootloader to Moto Devices
AppVersion={#AppVer}
VersionInfoVersion={#AppVer}
VersionInfoProductVersion={#AppVer}
VersionInfoProductTextVersion={#AppVer}
OutputBaseFilename=MotoFlasherSetup_{#AppVer}

[Icons]
Name: "{group}\MotoFlasher"; Filename: "{app}\MotoFlasher.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall MotoFasher"; Filename: "{uninstallexe}"
Name: "{group}\Visit My Site"; Filename: "http://negan1911.com"
Name: "{group}\Support"; Filename: "https://bitbucket.org/negan1911/motoflasher/issues"
Name: "{commondesktop}\MotoFlasher"; Filename: "{app}\MotoFlasher.exe"; WorkingDir: "{app}" 

[Run]
Filename: "{tmp}\dotNetFx40_Full_setup.exe"; Flags: waituntilterminated; Check: FrameworkIsNotInstalled
Filename: "{app}\MotoFlasher.exe"; Flags: postinstall nowait skipifsilent runascurrentuser; Description: "Launch application"
Filename: "{app}\MotoFlasher.exe"; Parameters: "-QUSBInstall"; Flags: waituntilterminated runascurrentuser; Description: "Install Driver"

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[UninstallRun]
Filename: "{app}\MotoFlasher.exe"; Parameters: "-QUSBUninstall"; WorkingDir: "{app}"; Flags: waituntilterminated skipifdoesntexist

[Code]
function FrameworkIsNotInstalled: Boolean;
begin
  Result := not RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\.NETFramework\policy\v4.0');
end;

procedure UpdateCheck;
begin
  DelTree(ExpandConstant('{app}'), True, True, True);
end;
