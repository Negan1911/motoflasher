﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Windows;

namespace MotoFlasher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// "/QUSBInstall" or "-QUSBInstall" Will Control Driver Installation No GUI.
    /// "/QUSBUninstall" or "-QUSBUninstall" Will Control Driver Uninstallation No GUI.
    /// "/CleanDriverConf" or "-CleanDriverConf" Will Return The Driver Config to his Original State.
    /// "/FullConfClean" or "-FullConfClean" Will Return All Config To his Original State.
    /// All Others Arguments Receive "Invalid Argument" MessageBox.
    /// </summary>
    public sealed partial class App
    {
        public static Updater Upd;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length >= 1)
            {
                for (var i = 0; i != e.Args.Length; ++i)
                {
                    /* This Will Paser CommandLine Arguments for Specified Tasks
                     * Like Install Or Uninstall Driver (Used By Installer). */
                    var cmd = new Classes.CommandLineArgs();
                    switch (e.Args[i])
                    {
                        case "/QUSBInstall":
                        case "-QUSBInstall":
                        {
                            var window = new MainWindow();
                            window.Show();
                            window.Hide();
                            cmd.InstallDriver();
                        }
                            break;
                        case "/QUSBUninstall":
                        case "-QUSBUninstall":
                        {
                            var window = new MainWindow();
                            window.Show();
                            window.Hide();
                            cmd.UninstallDriver();
                        }
                            break;
                        case "/CleanDriverConf":
                        case "-CleanDriverConf":
                        {
                            var window = new MainWindow();
                            window.Show();
                            window.Hide();
                            cmd.CleanDriverStore();
                        }
                            break;
                        case "/FullConfClean":
                        case "-FullConfClean":
                        {
                            var window = new MainWindow();
                            window.Show();
                            window.Hide();
                            cmd.CleanStore();
                        }
                            break;
                        default:
                        {
                            var window = new MainWindow();
                            window.Show();
                            window.Hide();
                            ModernDialog.ShowMessage("Invalid Arguments", "MotoFlasher Commandline:", MessageBoxButton.OK, window);
                            Environment.Exit(0);
                        }
                            break;
                    }
                }
            }
            else
            {
                /* Start Normal APP if aren't arguments */
                var window = new MainWindow();
                window.Show();
                Upd = new Updater("http://exxecsoft.com/downloads/MotoFlasherUpdater.xml", "MotoFlasher");
            }
        }
    }
}
