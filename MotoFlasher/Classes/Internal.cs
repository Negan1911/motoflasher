﻿using FirstFloor.ModernUI.Windows.Controls;
using MotoFlasher.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace MotoFlasher.Classes
{
    sealed class Internal
    {
        /// <summary>
        /// Handles All The Internal Process Operations on MotoFlasher
        /// </summary>
        private string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public void KillProcess()
        {
            /* Check with control if the Internal Service if running, show an alert to the user,
             * and kill the app if user press "Yes".*/
            Control p = new Control();
            if (p.isProcessRunning())
            {
                //If it running, send a Messagebox to the user, to make sure that the user wants to kill the app.
                MessageBoxResult killMessage = ModernDialog.ShowMessage("Warning: The Process is Running, don't stop it if it flashing. Do you want to stop it?", "Warning", MessageBoxButton.YesNo, MainWindow.AppWindow);
                if (killMessage == MessageBoxResult.Yes)
                {
                    //Kill "qBoot.exe" and "qFlash.exe" if the user push "Yes".
                    //qBoot Kill:
                    Process[] qBootRunning = Process.GetProcessesByName("qboot");
                    foreach (var processRunning in qBootRunning)
                    {
                        //Kill the Process and show a Message including the Process and PID for Each Process Running.
                        processRunning.Kill();
                        Flasher.AppWindow.WriteOnTextBox("WARNING:   The process stopped abnormaly (User Request)" + " Name: " + processRunning.ProcessName.ToString() + " PID: " + processRunning.Id.ToString());
                        p.activateProgressBar();
                        return;
                    }
                    //qFlash Kill:
                    Process[] qFlashRunning = Process.GetProcessesByName("qflash");
                    foreach (var processRunning in qFlashRunning)
                    {
                        //Kill the Process and show a Message including the Process and PID for Each Process Running.
                        processRunning.Kill();
                        Flasher.AppWindow.WriteOnTextBox("WARNING:   The process stopped abnormaly (User Request)" + " Name: " + processRunning.ProcessName.ToString() + " PID: " + processRunning.Id.ToString());
                        p.activateProgressBar();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        public bool KillProcessOnExit()
        {
            /* Check with control if the Internal Service if running, show an alert to the user,
             * and kill the app if user press "Yes", return bool to check if the app can exit or not.*/
            Control p = new Control();
            if (p.isProcessRunning())
            {
                //If it running, send a Messagebox to the user, to make sure that the user wants to kill the app.
                MessageBoxResult killMessage = ModernDialog.ShowMessage("Warning: The Process is Running, don't stop it if it flashing. Do you want to stop it?", "Warning", MessageBoxButton.YesNo, MainWindow.AppWindow);
                if (killMessage == MessageBoxResult.Yes)
                {
                    //Kill "qBoot.exe" and "qFlash.exe" if the user push "Yes"
                    //qBoot Kill:
                    Process[] qBootRunning = Process.GetProcessesByName("qboot");
                    foreach (var processRunning in qBootRunning)
                    {
                        //Kill the Process and show a Message including the Process and PID for Each Process Running.
                        processRunning.Kill();
                        Flasher.AppWindow.WriteOnTextBox("WARNING:   The process stopped abnormaly (User Request)" + " Name: " + processRunning.ProcessName.ToString() + " PID: " + processRunning.Id.ToString());
                        p.activateProgressBar();
                    }
                    //qFlash Kill:
                    Process[] qFlashRunning = Process.GetProcessesByName("qflash");
                    foreach (var processRunning in qFlashRunning)
                    {
                        //Kill the Process and show a Message including the Process and PID for Each Process Running.
                        processRunning.Kill();
                        Flasher.AppWindow.WriteOnTextBox("WARNING:   The process stopped abnormaly (User Request)" + " Name: " + processRunning.ProcessName.ToString() + " PID: " + processRunning.Id.ToString());
                        p.activateProgressBar();
                    }
                    //The app can close (false) if the processes are Killed.
                    return false;
                }
                //The app cannot close (true) if are process running.
                return true;
            }
            //The app can close (false) if aren't process running.
            return false;
        }
        public void RunWithRedirect(string cmdPath, string args)
        {
            /* Launch Programs, internals flashers, etc.
             * And redirect output to TextBox */
            var proc = new Process();
            proc.StartInfo.FileName = cmdPath;
            proc.StartInfo.WorkingDirectory = Path.GetDirectoryName(cmdPath);
            proc.StartInfo.Arguments = args;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.EnableRaisingEvents = true;
            proc.ErrorDataReceived += proc_DataReceived;
            proc.OutputDataReceived += proc_DataReceived;
            try
            {
                proc.Start();
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                ModernDialog.ShowMessage("File Not Found: \n Please, reinstall your program","Installation Corrupted",MessageBoxButton.OK,MainWindow.AppWindow);
                Flasher.AppWindow.WriteOnTextBox("EXEPTION: " + e.Message.ToString());
                return;
            }
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();
        }

        private void proc_DataReceived(object sender, DataReceivedEventArgs e)
        {
            //Handles the text and the progress bar from a secondary thread on the main application.
            Control p = new Control();
            if (Application.Current.Dispatcher.CheckAccess())
            {
                p.activateProgressBar();
                Flasher.AppWindow.WriteOnTextBox("OUTPUT:   " + e.Data);
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    p.activateProgressBar();
                    Flasher.AppWindow.WriteOnTextBox("OUTPUT:   " + e.Data);
                }));
            }
        }
    }
}
