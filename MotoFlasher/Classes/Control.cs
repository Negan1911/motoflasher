﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MotoFlasher.Pages;
using System.IO;
using System.Reflection;

namespace MotoFlasher.Classes
{
    sealed class Control
    {
        /// <summary>
        /// Handles all the Control Operation On MotoFlasher
        /// </summary>

        public void CheckRunningInstances()
        {
            /* This Method Will Control if the Application is Running
             * More than 1 time, and show a Messagebox if that happens */
            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            {
                MessageBoxResult p = ModernDialog.ShowMessage("The App is Actually Running...","Execution Error:", MessageBoxButton.OK,MainWindow.AppWindow);
                Environment.Exit(3);
            }
        }
        public void CheckAdminRights()
        {
            /* This Method Will Control if the Application is Running
             * Like Administrator, if isn't it will show a message */
            var AdminCheck = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            if (!AdminCheck.IsInRole(WindowsBuiltInRole.Administrator))
            {
                MessageBoxResult p = ModernDialog.ShowMessage("Please, Run the application with Administrator Permissions", "Execution Error:", MessageBoxButton.OK, MainWindow.AppWindow);
                Environment.Exit(4);
            }
        }
        public bool isThisWindows8orNewer()
        {
            /*Test if the Version os Windows is Equal to 8 or more Newer,
             * Return true if is 8 or More Newer. */
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            if (osInfo.Version.Major > 6)
            {
                //If Major Ver > 6, You Are using an OS Newer Than 8.
                return true;
            }
            else if ((osInfo.Version.Major == 6) && (osInfo.Version.Minor >= 2))
            {
                //If Major = 6 & Minor >=2, You Are Using Windows 8/8.1.
                return true;
            }
            /* If None of above sentences is True, You Are Running Windows 
             * 7/Vista/XP or Older (But Older Than XP Are Unsupported) */
            return false;
        }
        public void GUICheckWinVer()
        {
            /* Show a Message In The GUI If Windows Are >= 8, That is for 
             * Driver Installation. */
            bool runingWin8 = isThisWindows8orNewer();
            if (runingWin8 == true)
            {
                //Show Message if You Are Running Win8 or More Newer.
                Pages.Devices.AppWindow.Win8Link.Visibility = Visibility.Visible;
            }
            else
            {
                //Hide Message if You Are Running Win7 or Older.
                Pages.Devices.AppWindow.Win8Link.Visibility = Visibility.Collapsed;
            }
        }
        public void RoutinaryInit()
        {
            /*Control The Start of the app, and make the Routinary Task to properly Run the
             * Program, Writes on Textbox, Calls Log Folder Check, etc. */
            Flasher.AppWindow.GeneralProgressBar.IsIndeterminate = false;
            Flasher.AppWindow.WriteOnTextBox("**** BASIC INFORMATION ****");
            Flasher.AppWindow.WriteOnTextBox("SYSTEM:   Aplication Started at: " + DateTime.Now.ToString("dd/MM/yy hh:mm"));
            Flasher.AppWindow.WriteOnTextBox("SYSTEM:   OS: " + Environment.OSVersion);
            //Check if Are Running x64 or x86 Architecture and put it on TextBox.
            Flasher.AppWindow.WriteOnTextBox((System.Environment.Is64BitOperatingSystem) ? "ARCHITECTURE: x64" : "ARCHITECTURE: x86");
            //Put MotoFlasher Assembly Version.
            Flasher.AppWindow.WriteOnTextBox("ASSEMBLY:  MotoFlasher Version: " + Assembly.GetEntryAssembly().GetName().Version.ToString());
            Logger p = new Logger();
            p.CheckLogFolder();
            CheckPython27();
        }
        private void CheckPython27()
        {
            if (!File.Exists("C:\\Python27\\python.exe"))
            {
                /* Check if Python 2.7.X is installed and Disable Ghost (Moto X),
                 * because use .py file to flash */
                Flasher.AppWindow.WriteOnTextBox("WARNING: No Python 2.7.x detected on \"C:\\Python27\\\", Moto X flashing Disabled");
                Flasher.AppWindow.Ghost.IsEnabled = false;
            }
            if (!Directory.Exists("C:\\Python27\\Lib\\site-packages\\serial\\"))
            {
                /* Check if PySerial is installed and Disable Ghost (Moto X),
                 * because use .py file to flash */
                Flasher.AppWindow.WriteOnTextBox("WARNING: No PySerial detected on \"C:\\Python27\\\", Moto X flashing Disabled");
                Flasher.AppWindow.Ghost.IsEnabled = false;
            }
        }
        public void activateProgressBar(bool? activateNow = null)
        {
            /* Handle the ProgressBar, enable or disable if the internal Process is Running.
             * Use "Indeterminate" form to make it spin */
            if (activateNow != null)
            {
                Flasher.AppWindow.GeneralProgressBar.IsIndeterminate = (bool)activateNow;
            }
            else if (isProcessRunning())
            {
                Flasher.AppWindow.GeneralProgressBar.IsIndeterminate = true;
            }
            else
            {
                Flasher.AppWindow.GeneralProgressBar.IsIndeterminate = false;
            }
        }

        public bool isProcessRunning()
        {
            /* Check if the internal process are running to control some operations.
             * Return True if are running, false if not. */
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if ((clsProcess.ProcessName.Contains("qboot")) || (clsProcess.ProcessName.Contains("qflash")))
                {
                    return true;
                }
            }
            return false;
        }
        public string parseAndroidText()
        {
            /* Get the android version parsed but with text name, extract if from the Selected Value 
             * and pass it like text. */
            ComboBoxItem indexItem = (ComboBoxItem)Flasher.AppWindow.Version.SelectedValue;
            return indexItem.Content.ToString();
        }
        public int parseAndroidVersion()
        {
            /* Parse Android Version to call him from the Route of internal Ver.
             * Return a INT that is the version number without points (.) */
            ComboBoxItem indexItem = (ComboBoxItem)Flasher.AppWindow.Version.SelectedValue;
            string androidVer = indexItem.Content.ToString();
            if (androidVer == "4.2.2")
            {
                return 442;
            }
            else if (androidVer == "4.3")
            {
                return 43;
            }
            else if (androidVer == "4.4")
            {
                return 44;
            }
            else if (androidVer == "4.4.2")
            {
                return 442;
            }
            else if (androidVer == "4.4.4")
            {
                return 444;
            }
            else if (androidVer == "5.0/5.0.X")
            {
                return 50;
            }
            else
            {
                return 0;
            }
        }
    }
}
