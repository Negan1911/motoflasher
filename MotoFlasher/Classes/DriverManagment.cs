﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace MotoFlasher.Classes
{
    sealed class DriverManagment
    {

        private string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public void CleanDriver(bool isCommandLine)
        {
            /* This Uninstall All The Old Qualcomm Drivers That Don't
             * Update with DPInst if are Installed. */
            //isCommandLine Check if is runned from Command Line or GUI
            if (isCommandLine == true)
            {
                DriverDelete("05C6 9008");
                DriverDelete("05c6 9008");
                }
            else
            {
                Pages.Devices.AppWindow.ProgessBar.IsIndeterminate = true;
                DriverDelete("05C6 9008");
                DriverDelete("05c6 9008");
                Pages.Devices.AppWindow.ProgessBar.IsIndeterminate = false;
                Pages.Devices.AppWindow.ProgessBar.Value = 100;
            }
        }
        public bool IsUsbDeviceConnected(string vid, string pid = null)
        {
            /* Use Managment to check if a Device is Connected with PID & Vid. */
            ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_USBControllerDevice");

            foreach (ManagementObject queryObj in searcher.Get())
            {
                var usbDevice = Convert.ToString(queryObj);
                if (usbDevice.Contains(pid) && usbDevice.Contains(vid))
                    return true;
            }
            return false;
        }

        public int phoneDetection()
        {
            /* This will detect how your device is connected on the PC, so
             * got back an INT if the device is Recognized, and with Driver:
             * Returns Values:
             *      0       No Device Detected.
             *      1       QCOM Device without Driver.
             *      -       QCOM Device With Driver.
             *      3       Fastboot Device.
             */
            bool qcom = IsUsbDeviceConnected("05C6", "9008");
            bool fbot = IsUsbDeviceConnected("22B8", "2E80");
            if (qcom == true)
            {
                return 1;
            }
            else if (fbot == true)
            {
                //Fastboot Detected, Now Detect if Driver is Installed.
                return 3;
            }
            else
            {
                //No devices Found, neither QUSB or Fastboot.
                return 0;
            }
        }
        private void DriverDelete(string vidpid)
        {
            /* Driver Delete Internal Execution for CleanDriver Method. */
            Process proc = new Process();
            proc.StartInfo.FileName = this.DirPath + "\\Driver\\clean.exe";
            proc.StartInfo.WorkingDirectory = this.DirPath + "\\Driver\\";
            proc.StartInfo.Arguments = vidpid;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
        }
        public int InstallDriver()
        {
            /* Install Respective Version Of the driver for 64 or 32 Bits
             * With DPInst Installer For All Windows Version. */
            Process proc = new Process();
            if (System.Environment.Is64BitOperatingSystem)
            {
                //Run 64 Bit Installation if This is True.
                proc.StartInfo.FileName = this.DirPath + "\\Driver\\x64\\dpinst.exe";
                proc.StartInfo.WorkingDirectory = this.DirPath + "\\Driver\\x64\\";
            }
            else
            {
                //Run 32 Bit Installation if This is False.
                proc.StartInfo.FileName = this.DirPath + "\\Driver\\x86\\dpinst.exe";
                proc.StartInfo.WorkingDirectory = this.DirPath + "\\Driver\\x86\\";
            }
            //Start The Process.
            proc.StartInfo.Arguments = "/el /f /lm /sa /se /sw";
            proc.Start();
            proc.WaitForExit();
            //Return ExitCode for Error Checking.
            return proc.ExitCode;
        }

        public void TestModeControl(bool switcher)
        {
            string arguments;
            string arguments2;
            string cmdFullFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows),
                                       Environment.Is64BitOperatingSystem && !Environment.Is64BitProcess
                                           ? @"Sysnative\cmd.exe" : @"System32\cmd.exe");
            if (switcher == true)
            {
                //Enable DPInst Unsigned Flag, StartInfo for TestSigning and Kernel Debug.
                Environment.SetEnvironmentVariable("_DFX_INSTALL_UNSIGNED_DRIVER", "1", EnvironmentVariableTarget.Machine);
                arguments = "-set TESTSIGNING ON";
                arguments2 = "-debug ON";

            }
            else
            {
                //Disable DPInst Unsigned Flag, StartInfo for TestSigning and Kernel Debug.
                Environment.SetEnvironmentVariable("_DFX_INSTALL_UNSIGNED_DRIVER", "0", EnvironmentVariableTarget.Machine);
                arguments = "-set TESTSIGNING OFF";
                arguments2 = " -debug OFF";
            }
            ProcessStartInfo psi = new ProcessStartInfo(cmdFullFileName, "/c bcdedit " + arguments)
            {
                UseShellExecute = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            };
            ProcessStartInfo psi2 = new ProcessStartInfo(cmdFullFileName, "/c bcdedit " + arguments2)
            {
                UseShellExecute = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            };
            var process = new Process { StartInfo = psi };
            var process2 = new Process { StartInfo = psi2 };
            process.Start();
            process2.Start();
            process.WaitForExit();
            process2.WaitForExit();
        }

        public void SetDriverStatus(bool Acc)
        {
            /*Set Driver Status Stored in Internal Config */
            Settings.SetDriverInstalled(Acc);
        }
        public bool GetDriverStatus()
        {
            return Settings.GetDriverInstalled();
       }
    }
}
