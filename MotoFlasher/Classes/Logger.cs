﻿using MotoFlasher.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoFlasher.Classes
{
    sealed class Logger
    {
        /// <summary>
        /// Handles all the Logging Operation on MotoFlasher
        /// </summary>
        private string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public void CheckLogFolder()
        {
            /* Check if the "Logs" folder exist on the Program Path,
             * if doesn't exist, it will create it. */
            if (!Directory.Exists(this.DirPath + "\\Logs"))
            {
                Directory.CreateDirectory(this.DirPath + "\\Logs");
                Flasher.AppWindow.WriteOnTextBox("LOGGING:   LOG FOLDER CREATED");
            }
        }
        public void DumpLog()
        {
            /* Dump all the TextBox when the app is closing and save it to a new .log file.
             * Is saved with DD-MM-YY-HH-MM_(number of execution on the same hour here). */
            string fileName = "log_";
            int fileNumber = 00;
            Flasher.AppWindow.WriteOnTextBox("**** END OF PROGRAM ****");
            do
            {
                fileNumber++;
                fileName = "log_" + DateTime.Now.ToString("dd-MM-yy-hh-mm" + "_" + fileNumber.ToString());
            }
            while (File.Exists(this.DirPath + "\\Logs\\" + fileName + ".log"));
            File.WriteAllText(this.DirPath + "\\Logs\\" + fileName + ".log", Flasher.AppWindow.FlasherBox.Text.ToString());
        }
        public void OpenLogFolder()
        {
            /* Open a File Explorer With the Log folder */
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = Environment.GetEnvironmentVariable("WINDIR") + @"\explorer.exe";
            prc.StartInfo.Arguments = this.DirPath + "\\Logs";
            prc.Start();
        }
    }
}
