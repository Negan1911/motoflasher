﻿using FirstFloor.ModernUI.Presentation;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Media;
using System.Xml;

namespace MotoFlasher.Classes
{
    static class Settings
    {
        static public void CleanKey()
        {
            // Delete registry!
            Registry.CurrentUser.DeleteSubKey(@"SOFTWARE\MotoFlasher");
        }
        static public bool GetDriverInstalled()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\MotoFlasher");
            try
            {
                if (key.GetValue("DriverInstalled").ToString() == "y")
                {
                    return true;
                }
                else return false;
            }
            catch
            {
                return false;
            }
        }

        static public void SetDriverInstalled(bool p)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\MotoFlasher");
            
            if (p == true)
            {
                key.SetValue("DriverInstalled", "y");
            }
            else
            {
                key.SetValue("DriverInstalled", "n");
            }

            key.Close();
        }
    }
}
