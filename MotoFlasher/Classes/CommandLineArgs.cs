﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace MotoFlasher.Classes
{
    sealed class CommandLineArgs
    {
        /// <summary>
        /// This Will Handle All CommandLine Like: Install/ Uninstall Driver, Clear Driver Config, and Clean Full Config
        /// </summary>
        private System.Windows.Forms.NotifyIcon MyNotifyIcon;

        public CommandLineArgs()
        {
            // Constructor will initialize and create a Notify Icon.
            MyNotifyIcon = new System.Windows.Forms.NotifyIcon();
            MyNotifyIcon.Icon = Properties.Resources.MotoFlasher;
        }
        public void InstallDriver()
        {
            /* This Will Install The Driver With CommandLine Argument */
            Control p = new Control();
            //Check if The App is Running 2 Times.
            p.CheckRunningInstances();
            //Check if The App is Running Like Admin.
            p.CheckAdminRights();
            //Notify Traybar Enable.
            this.MyNotifyIcon = new System.Windows.Forms.NotifyIcon();
            //In App Resource Icon.
            this.MyNotifyIcon.Icon = Properties.Resources.Update;
            //Win 8 Check and Driver Install Check
            DriverManagment dm = new DriverManagment();
            if ((p.isThisWindows8orNewer() == false)&&(dm.GetDriverStatus() == false))
            {
                /* If Windows 7 or Older, we can proceed With Silent Installation,
                 * This Will Handle All the Process */
                //Show TrayBar Icon.
                this.MyNotifyIcon.Visible = true;
                //Ballon Show.
                this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
                this.MyNotifyIcon.BalloonTipText = "Installing Drivers!";
                this.MyNotifyIcon.ShowBalloonTip(400);
                //Clean Old Drivers.
                dm.CleanDriver(true);
                //Install New Drivers.
                int exitCode = dm.InstallDriver();
                //Check Exit Code For Errors.
                if ((exitCode != 0) && (exitCode != 256) && (exitCode != 768))
                {
                    /* Driver Installation Failed */
                    ModernDialog.ShowMessage("The Driver Installation Probably Failed, Please, Open DriverInstaller Manually and try installing with Test Mode. Code: " + exitCode.ToString(), "DriverInstaller", MessageBoxButton.OK, MainWindow.AppWindow);
                    Environment.Exit(0);
                }
                else
                {
                    /* Driver Installation Completed */
                    dm.SetDriverStatus(true);
                    this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
                    this.MyNotifyIcon.BalloonTipText = "SucessFully Installed!";
                    this.MyNotifyIcon.ShowBalloonTip(400);
                    Thread.Sleep(9000);
                    this.MyNotifyIcon.Visible = false;
                    this.MyNotifyIcon.Dispose();
                    Environment.Exit(0);
                }
            }
            else if (p.isThisWindows8orNewer() == true)
            {
                /* Error If Windows 8 or Newer is Trying to install the driver */
                ModernDialog.ShowMessage("Windows 8 or Newer OS need a Manual Driver Installation, for More Info Open DriverInstaller", "MotoFlasher", MessageBoxButton.OK);
                this.MyNotifyIcon.Visible = false;
                this.MyNotifyIcon.Dispose();
                Environment.Exit(0);
            }
            else if (dm.GetDriverStatus() == true)
            {
                /* The Driver is Already Installed */
                this.MyNotifyIcon.Visible = true;
                this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
                this.MyNotifyIcon.BalloonTipText = "The Driver Is Already Installed, use MotoFlasher to Reinstall or Clean Settings.";
                this.MyNotifyIcon.ShowBalloonTip(400);
                Thread.Sleep(9000);
                this.MyNotifyIcon.Visible = false;
                this.MyNotifyIcon.Dispose();
                Environment.Exit(0);
            }
        }
        public void UninstallDriver ()
        {
            /*This Will Uninstall Driver With the CommandLine Argument */
            //Show TrayBar Icon.
            
            this.MyNotifyIcon.Visible = true;
            DriverManagment dm = new DriverManagment();
            //Uninstall Drivers.
            dm.CleanDriver(true);
            dm.SetDriverStatus(false);
            //Ballon Show.
            this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
            this.MyNotifyIcon.BalloonTipText = "Uninstall Completed";
            this.MyNotifyIcon.ShowBalloonTip(400);
            Thread.Sleep(9000);
            this.MyNotifyIcon.Visible = false;
            this.MyNotifyIcon.Dispose();
            Environment.Exit(0);
        }
        public void CleanDriverStore()
        {
            MessageBoxResult j = ModernDialog.ShowMessage("Are You Sure That You Want to Reset The Driver Stored Settings ?", "Configuration Store", MessageBoxButton.YesNo, MainWindow.AppWindow);
            if (j == MessageBoxResult.Yes)
            {

                /* Reset Driver Configuration */
                this.MyNotifyIcon.Visible = true;
                Settings.SetDriverInstalled(false);
                this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
                this.MyNotifyIcon.BalloonTipText = "Sucessfully Erased Driver Configuration!";
                this.MyNotifyIcon.ShowBalloonTip(400);
                Thread.Sleep(9000);
                this.MyNotifyIcon.Visible = false;
                this.MyNotifyIcon.Dispose();
                Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }
        public void CleanStore()
        {
            MessageBoxResult j = ModernDialog.ShowMessage("Are You Sure That You Want to Reset The Stored Settings (Doesn't Affect to Driver State)?", "Configuration Store", MessageBoxButton.YesNo,MainWindow.AppWindow);
            if (j == MessageBoxResult.Yes)
            {
                /* Reset Store Configuration */
                this.MyNotifyIcon.Visible = true;
                bool DrvInst = Settings.GetDriverInstalled();
                Settings.CleanKey();
                Settings.SetDriverInstalled(DrvInst);
                this.MyNotifyIcon.BalloonTipTitle = "MotoFlasher";
                this.MyNotifyIcon.BalloonTipText = "Sucessfully Erased Stored Configuration!";
                this.MyNotifyIcon.ShowBalloonTip(400);
                Thread.Sleep(9000);
                this.MyNotifyIcon.Visible = false;
                this.MyNotifyIcon.Dispose();
                Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }
}
