import platform, os, subprocess, sys

def find_dload_port(dloat_port_desc):
	dload_port_found = False
	dload_port = ""

	port_list = list(serial.tools.list_ports.comports())
	''' Loop through the available ports to find the COM port
		used for dload enumeration.
		Exit if multiple dload enumerations are found '''
	for port in port_list:
		if dload_port_desc in port[1]:
			if not dload_port_found:
				dload_port_found = True
				dload_port = port[0]
			else:
				print("Multiple dload enumeration found!\n")
				print("Aborting blank flash!")
				exit()
	if dload_port_found:
		return dload_port.lower()
	else:
		print("Emergency download port not detected!")
		exit()

def check_dependencies(dependency_list):
	file_list = set(os.listdir('.'))
	if dependency_list.issubset(file_list):
		return True
	else:
		missing_files = dependency_list.difference(file_list)
		print("The following files are missing!")
		for file in missing_files:
			print(file)
		return False

def execute_qflash(port, qflash, rdl_file, singleimage_file):
		command = qflash + ' -' + port + ' -ramload ' + rdl_file + ' -mbn 33 ' + singleimage_file + ' -v -o'
		print("Executing command " + command)
		return os.system(command)

if __name__ == "__main__":

	#Verify python version is > 2.6 and < 3.0
	python_version = sys.version_info
	if python_version[0] != 2 or python_version[1] < 6:
		print("Python version 2.6 or 2.7 required!")
		print("you can download Python 2.6.x or 2.7.x here:")
		print("http://www.python.org/download/releases/")
		exit()

	# check whether pyserial 2.6 is installed.
	try:
		import serial.tools.list_ports
	except ImportError:
		print("Please install pyserial 2.6 from here")
		print("https://pypi.python.org/pypi/pyserial")
		exit()

	dload_port_desc = "Qualcomm HS-USB QDLoader 9008"
	''' files required for blank flashing.
		will be available as part of the blank flash package'''
	rdl_file = "MPRG8960.hex"
	qflash_file = "qflash.exe"
	singleimage_file = "MSM8960_bootloader_singleimage.bin"
	dependency_list = set([rdl_file, qflash_file, singleimage_file])

	if platform.system() != 'Windows':
		print("Qcom blank flashing supported only on windows!\n")
		exit()
	# Find the emergency download COM port
	port = find_dload_port(dload_port_desc)
	print("Emergency download enumeration detected on port - " + port)

	''' check if all the required files are present in the current directory
		exit is any file is missing'''
	if  not check_dependencies(dependency_list):
		exit()
	print("Starting qflash!")
	if execute_qflash(port, qflash_file, rdl_file, singleimage_file) == 0:
		print("Blank flashing successful")
		print("Device will now enumerate in fastboot mode")
	else:
		print("Blank flashing failed\n")
