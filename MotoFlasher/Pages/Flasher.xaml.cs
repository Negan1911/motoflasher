﻿using MotoFlasher.Classes;
using MotoFlasher.Devices;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MotoFlasher.Pages
{
    /// <summary>
    /// Interaction logic for Flasher.xaml
    /// </summary>
    public sealed partial class Flasher
    {
        public static Flasher AppWindow;
        public Flasher()
        {
            InitializeComponent();
            AppWindow = this;
            var p = new Classes.Control();
            p.RoutinaryInit();
            CheckDeviceConnected();
        }

        public void WriteOnTextBox (string p)
        {
            /*This Method Will Properly Add Text to the Debug Textbox,
             * With Scroll To End and Auto NewLine */
            FlasherBox.AppendText(p + Environment.NewLine);
            FlasherBox.ScrollToEnd();
        }
        private void DevicesChanged(object sender, SelectionChangedEventArgs e)
        {
            /* Event that control the Device Changed, it will check that you don't
             * select an incorrect android version for your device. */
            //Return Index To Null if you change the device
            Version.SelectedIndex = -1;
            if (Devices.SelectedIndex != -1)
            {
                Version.IsEnabled = true;
            }
            //Enable / Disable Items For The Devices.
            if (Falcon.IsSelected)
            {
                var f = new Falcon();
                f.DeviceSelection();
            }
            else if (Ghost.IsSelected)
            {
                var g = new Ghost();
                g.DeviceSelection();
            }
            Detect.IsEnabled = true;
        }
        private void VersionsChanged(object sender, SelectionChangedEventArgs e)
        {
            /* Event that control the Android Version Changed, and enable flash
             * button if you complete the selection */
            Flash.IsEnabled = Version.SelectedIndex != -1;
        }

        private void CheckDeviceConnected(object sender = null, RoutedEventArgs e = null)
        {
            var dm = new DriverManagment();
            var p = dm.phoneDetection();
            switch (p)
            {
                case 0:
                    DetText.Text = "NOT CONNECTED";
                    DetText.Foreground = Brushes.Red;
                    break;
                case 1:
                    DetText.Text = "QCOM DETECTED";
                    DetText.Foreground = Brushes.Blue;
                    break;
                case 3:
                    DetText.Text = "FASTBOOT DETECTED";
                    DetText.Foreground = Brushes.Green;
                    break;
            }
        }
        private void DevManButton_Click(object sender, RoutedEventArgs e)
        {
            var proc = new Process
            {
                StartInfo = {FileName = Environment.SystemDirectory + "\\mmc.exe", Arguments = "devmgmt.msc"}
            };
            proc.Start();
        }
        private void Detect_Click(object sender, RoutedEventArgs e)
        {
            /*Start Detection For The Device Selected */
            var i = new Internal();
            i.KillProcess();
            if (Falcon.IsSelected)
            {
                var f = new Falcon();
                f.RunDetection();
            }
            else if (Ghost.IsSelected)
            {
                var g = new Ghost();
                g.RunDetection();
            }
        }
        private void Flash_Click(object sender, RoutedEventArgs e)
        {
            /* Start Flashing For The Device Selected, and check that 
             * aren't processes running. */
            var i = new Internal();
            i.KillProcess();
            if (Falcon.IsSelected)
            {
                var f = new Falcon();
                f.Flash();
            }
            else if (Ghost.IsSelected)
            {
                var g = new Ghost();
                g.Flash();
            }
        }
        private void OpenLog_Click(object sender, RoutedEventArgs e)
        {
            /* Control Log Button Pressing */
            var l = new Logger();
            l.OpenLogFolder();
        }
    }
}
