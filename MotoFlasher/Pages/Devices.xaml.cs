﻿using FirstFloor.ModernUI.Windows.Controls;
using MotoFlasher.Classes;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace MotoFlasher.Pages
{
    /// <summary>
    /// Interaction logic for Devices.xaml
    /// </summary>
    public sealed partial class Devices
    {
        public static Devices AppWindow;
        public Devices()
        {
            InitializeComponent();
            AppWindow = this;
            CheckDeviceConnected();
            var dm = new DriverManagment();
            var c = new Control();
            DriverInstalled.IsChecked = dm.GetDriverStatus();
            EnableDisableWin8Link(c.isThisWindows8orNewer());
            CheckPhyton();
        }

        private void Uninstall_Click(object sender, RoutedEventArgs e)
        {
            /* Handle GUI Uninstall and Call Internal Method */
            StatusDescription.Text = "Uninstalling Drivers...";
            Install.IsEnabled = false;
            Uninstall.IsEnabled = false;
            var dm = new DriverManagment();
            var c = new Control();
            dm.CleanDriver(false);
            ProgessBar.Foreground = Brushes.Green;
            ProgessBar.Value = 100;
            Install.IsEnabled = true;
            Uninstall.IsEnabled = true;
            StatusDescription.Text = "Uninstalled Sucessfully!";
            if (c.isThisWindows8orNewer() == false)
            {
                dm.SetDriverStatus(false);
            }
            DriverInstalled.IsChecked = dm.GetDriverStatus();
        }
        private void Install_Click(object sender, RoutedEventArgs e)
        {
            /* Handle GUI Install and Call Internal Method */
            //Firs GUI Config.
            StatusDescription.Text = "Please, Check the Driver Wizard";
            ProgessBar.Value = 100;
            var dm = new DriverManagment();
            var c = new Control();
            dm.CleanDriver(false);
            ProgessBar.Value = 100;
            //Run Installation
            var exitCode = dm.InstallDriver();
            dm.SetDriverStatus(true);
            if ((exitCode != 0) && (exitCode != 256) && (exitCode != 768))
            {
                //GUI For Driver Installation Failed.
                ProgessBar.Foreground = Brushes.Red;
                StatusDescription.Text = "The Driver Installation Probably Failed, Please, Open DriverInstaller Manually and try installing with Test Mode. Code: " + exitCode;
            }
            else
            {
                //GUI For Driver Installation Completed.
                ProgessBar.Value = 100;
                ProgessBar.Foreground = Brushes.Green;
                StatusDescription.Text = "Installation Completed";
                if (c.isThisWindows8orNewer() == false)
                {
                    dm.SetDriverStatus(true);
                }
            }
            DriverInstalled.IsChecked = dm.GetDriverStatus();
        }
        private void DisableTM_Click(object sender, RoutedEventArgs e)
        {
            /* Disable TestMode GUI Control. */
            var dm = new DriverManagment();
            var s = new ShutDown();
            //Internal Method to Disable TestMode.
            dm.TestModeControl(false);
            //MessageBox To Reboot.
            var f = ModernDialog.ShowMessage("You will need to reboot the pc before you can apply changes. You want to Reboot?", "Changes Applied", MessageBoxButton.YesNo, MainWindow.AppWindow);
                if (f == MessageBoxResult.Yes)
                {
                    //If User Press Yes, Reboot the PC.
                    s.RebootPC();
                }
        }
        private void EnableTM_Click(object sender, RoutedEventArgs e)
        {
            /* Enable TestMode GUI Control. */
            var dm = new DriverManagment();
            var s = new ShutDown();
            //Internal Method to Disable TestMode.
            dm.TestModeControl(true);
            //MessageBox To Reboot.
            var f = ModernDialog.ShowMessage("You will need to reboot the pc before you can install the driver. You want to Reboot?", "Changes Applied", MessageBoxButton.YesNo, MainWindow.AppWindow);
            if (f == MessageBoxResult.Yes)
                {
                    //If User Press Yes, Reboot the PC.
                    s.RebootPC();
                }
        }
        private void Win8Plus_Click(object sender, RoutedEventArgs e)
        {
            /* Enable PopUp When Click Win8 Driver Override Link. */
            var w8A = new Windows8Advice();
            w8A.Show();
        }
        private void CheckDeviceConnected(object sender = null, RoutedEventArgs e = null)
        {
            var dm = new DriverManagment();
            var p = dm.phoneDetection();
            switch (p)
            {
                case 0:
                    DetText.Text = "NOT CONNECTED";
                    DetText.Foreground = Brushes.Red;
                    break;
                case 1:
                    DetText.Text = "QCOM DETECTED";
                    DetText.Foreground = Brushes.Blue;
                    break;
                case 3:
                    DetText.Text = "FASTBOOT DETECTED";
                    DetText.Foreground = Brushes.Green;
                    break;
            }
        }
        private void DevManButton_Click(object sender, RoutedEventArgs e)
        {
            var proc = new Process
            {
                StartInfo = {FileName = Environment.SystemDirectory + "\\mmc.exe", Arguments = "devmgmt.msc"}
            };
            proc.Start();
        }

        private void EnableDisableWin8Link(bool activation)
        {
            if (activation)
            {
                Win8Link.IsEnabled = true;
                Win8Link.Visibility = Visibility.Visible;
            }
            else
            {
                Win8Link.IsEnabled = false;
                Win8Link.Visibility = Visibility.Collapsed;
            }

        }

        private void CheckPhyton()
        {
            if (!File.Exists("C:\\Python27\\python.exe"))
            {
                PhytonCheck.IsChecked = false;
            }
            if (!Directory.Exists("C:\\Python27\\Lib\\site-packages\\serial\\"))
            {
                PySerialCheck.IsChecked = false;
            }
        }
    }
}
