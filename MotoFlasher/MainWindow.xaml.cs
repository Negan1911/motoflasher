﻿using FirstFloor.ModernUI.Presentation;
using MotoFlasher.Classes;
using System;

namespace MotoFlasher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public sealed partial class MainWindow
    {
        public static MainWindow AppWindow;
        public MainWindow()
        {
            /* Basic Controls and Prepare the App for Execution.
             * Check Rights, Thread, Initialize Components,
             * Prepare In place things, Check Updates, etc. */
            var p = new Control();
            AppWindow = this;
            InitializeComponent();
            Show();
            p.CheckRunningInstances();
            p.CheckAdminRights();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            /* Run Steps Before Closing the App, like checking if process is running, dump
             * log file, closing update window */
            var p = new Internal();
            var q = new Logger();
            e.Cancel = p.KillProcessOnExit();
            q.DumpLog();
            App.Upd.ReceiveKillApp();
            Environment.Exit(0);
        }
    }
}
