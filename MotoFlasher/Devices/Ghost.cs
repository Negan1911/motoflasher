﻿using Microsoft.Win32;
using MotoFlasher.Classes;
using MotoFlasher.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MotoFlasher.Devices
{
    sealed class Ghost : DevInfo
    {
        /// <summary>
        /// Library That Handles all 'Ghost' (Moto X) Interaction
        /// </summary>

        public override void DeviceSelection()
        {
            /* Control the View of Buttons Enabled / Disabled for Moto G selection,
             * And make it invisible or visible.
             * This is Called When the user click 'Moto X' on Devices List. */

            //Enables the selection of the following android versions.
            Flasher.AppWindow.A422.IsEnabled = true;
            Flasher.AppWindow.A43.IsEnabled = false;
            Flasher.AppWindow.A44.IsEnabled = true;
            Flasher.AppWindow.A442.IsEnabled = true;
            Flasher.AppWindow.A444.IsEnabled = false;
            Flasher.AppWindow.A50.IsEnabled = false;
            Flasher.AppWindow.Custom.IsEnabled = true;
            //Disable or Enable visual items
            Flasher.AppWindow.A422.Visibility = Visibility.Visible;
            Flasher.AppWindow.A43.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A44.Visibility = Visibility.Visible;
            Flasher.AppWindow.A442.Visibility = Visibility.Visible;
            Flasher.AppWindow.A444.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A50.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.Custom.Visibility = Visibility.Visible;
        }
        public override void RunDetection()
        {
            /* Run Device Detection With qFlash.exe, this will show if the device 
             * is correctly attached to the PC, and working good with drivers. */
            Control c = new Control();
            c.activateProgressBar();
            Flasher.AppWindow.WriteOnTextBox("");
            Flasher.AppWindow.WriteOnTextBox("****DETECTION STARTED:****");
            //Search the directory where a basic "qflash.exe" is, to call 'devices' mode.
            //Use: Dirpath is the exection place + Route where the files are.
            string ExecPath = this.DirPath + "\\Component\\ghost_422\\qflash.exe";
            //Print Paths
            Flasher.AppWindow.WriteOnTextBox("EXEC: " + ExecPath);
            Flasher.AppWindow.WriteOnTextBox("DIR: " + this.DirPath);
            Internal i = new Internal();
            //Use: RunWithRedirect(String ExecutableFile, String Args )
            i.RunWithRedirect(ExecPath, "-portlist");
            //Disable ProgresBar Directly
            c.activateProgressBar(false);
        }
        public override void Flash()
        {
            /* Run flash for Moto X, Select the Correct Android Version, make Some Steps,
             * and let the job to the Python file (8960_blankflash.py) */
            Control c = new Control();
            Internal i = new Internal();
            Flasher.AppWindow.WriteOnTextBox("");
            if (Flasher.AppWindow.Custom.IsSelected)
            {
                //Run if the user don't select and android version, and select "custom flasher".

                Flasher.AppWindow.WriteOnTextBox("SYSTEM:   Device Selected: Moto X v1, Custom Android Flasher");
                Flasher.AppWindow.WriteOnTextBox("**** QBOOT SELECTION STARTED: ****");

                //Run Cusom *.py selector, and flash it from the user selection. 
                OpenFileDialog openFileDialog = new OpenFileDialog();
                //Phyton file is the default.
                openFileDialog.Filter = "Python BlankFlash (8960_blankflash.py)|8960_blankflash.py|Python File (*.py)|*.py";
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if (openFileDialog.ShowDialog() == true)
                {
                    //Sends the user selected route and arguments, after this, start flashing.
                    Flasher.AppWindow.WriteOnTextBox("**** BOOTLOADER FLASHING STARTED: ****");
                    //Use: RunWithRedirect(String ExecutableFile, String Args )
                    i.RunWithRedirect("C:\\Python27\\python.exe", "\"" + openFileDialog.FileName + "\"");
                }
            }
            else
            {
                //Run if the user select an android version to flash, that is included in the program.
                Flasher.AppWindow.WriteOnTextBox("SYSTEM:   Device Selected: Moto X v1, Android " + c.parseAndroidText());
                Flasher.AppWindow.WriteOnTextBox("**** BOOTLOADER FLASHING STARTED: ****");
                //Parse android sending the route and arguments, after this, start flashing.
                //Use: Dirpath is the execution place + "\\Component\\yourcomponent_" + c.parseAndroidVersion() + "\\qboot.exe".
                //ParseAndroidVersion will get the android version path like the list explained.
                string ExecPath = this.DirPath + "\\Component\\ghost_" + c.parseAndroidVersion() + "\\8960_blankflash.py";
                //Print Paths
                Flasher.AppWindow.WriteOnTextBox("EXEC: " + ExecPath);
                Flasher.AppWindow.WriteOnTextBox("DIR: " + this.DirPath);
                //Use: RunWithRedirect(String ExecutableFile, String Args )
                i.RunWithRedirect("C:\\Python27\\python.exe", "\"" + ExecPath + "\"");
            }
            //Disable ProgresBar Directly
            c.activateProgressBar(false);
        }
    }
}
