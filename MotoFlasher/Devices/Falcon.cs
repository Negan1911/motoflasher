﻿using Microsoft.Win32;
using MotoFlasher.Classes;
using MotoFlasher.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MotoFlasher.Devices
{
    sealed class Falcon : DevInfo
    {
        /// <summary>
        /// Library that handles all 'falcon' (Moto G) Interaction.
        /// </summary>
        
        public override void DeviceSelection()
        {
            /* Control the View of Buttons Enabled / Disabled for Moto G selection,
             * And make it invisible or visible.
             * This is Called When the user click 'Moto G' on Devices List. */

            //Enables the selection of the following android versions.
            Flasher.AppWindow.A422.IsEnabled = false;
            Flasher.AppWindow.A43.IsEnabled = true;
            Flasher.AppWindow.A44.IsEnabled = false;
            Flasher.AppWindow.A442.IsEnabled = true;
            Flasher.AppWindow.A444.IsEnabled = true;
            Flasher.AppWindow.A50.IsEnabled = false;
            Flasher.AppWindow.Custom.IsEnabled = true;
            //Disable or Enable visual items
            Flasher.AppWindow.A422.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A43.Visibility = Visibility.Visible;
            Flasher.AppWindow.A44.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A442.Visibility = Visibility.Visible;
            Flasher.AppWindow.A444.Visibility = Visibility.Visible;
            Flasher.AppWindow.A50.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.Custom.Visibility = Visibility.Visible;
        }

        public override void RunDetection()
        {
            /* Run Device Detection With qBoot.exe, this will show if the device 
             * is correctly attached to the PC, and working good with drivers. */
            Control c = new Control();
            //Make Visual Functions First.
            c.activateProgressBar();
            //Write Some text on the Main Box
            Flasher.AppWindow.WriteOnTextBox("");
            Flasher.AppWindow.WriteOnTextBox("****DETECTION STARTED:****");
            //Search the directory where a basic "qboot.exe" is, to call 'devices' mode.
            //Use: Dirpath is the exection place + Route where the files are.
            string ExecPath = this.DirPath + "\\Component\\falcon_43\\qboot.exe";
            // Print Paths
            Flasher.AppWindow.WriteOnTextBox("EXEC: " + ExecPath);
            Flasher.AppWindow.WriteOnTextBox("DIR: " + this.DirPath);
            Internal i = new Internal();
            //Use: RunWithRedirect(String ExecutableFile, String Args )
            i.RunWithRedirect(ExecPath, "devices");
            //Disable ProgresBar Directly
            c.activateProgressBar(false);
        }

        public override void Flash()
        {
            /* Run flash for Moto G, Select the Correct Android Version, make Some Steps,
             * and let the job to the internal service (qboot.exe) */
            Control c = new Control();
            Internal i = new Internal();
            Flasher.AppWindow.WriteOnTextBox("");
            if (Flasher.AppWindow.Custom.IsSelected)
            {
                //Run if the user don't select and android version, and select "custom flasher".

                Flasher.AppWindow.WriteOnTextBox("SYSTEM:   Device Selected: Moto G v1, Custom Android Flasher");
                Flasher.AppWindow.WriteOnTextBox("**** QBOOT SELECTION STARTED: ****");

                //Run Cusom qboot selector, and flash it from the user selection. 
                OpenFileDialog openFileDialog = new OpenFileDialog();
                //Motorola Qboot is by default.
                openFileDialog.Filter = "Motorola QBoot (qboot.exe)|qboot.exe|All Executables (*.exe)|*.exe";
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if(openFileDialog.ShowDialog() == true)
                {
                    //Sends the user selected route and arguments, after this, start flashing.
                    Flasher.AppWindow.WriteOnTextBox("**** BOOTLOADER FLASHING STARTED: ****");
                    //Use: RunWithRedirect(String ExecutableFile, String Args )
                    i.RunWithRedirect(openFileDialog.FileName, "blank-flash");
                }
            }
            else
            {
                //Run if the user select an android version to flash, that is included in the program.
                Flasher.AppWindow.WriteOnTextBox("SYSTEM:   Device Selected: Moto G v1, Android " + c.parseAndroidText());
                Flasher.AppWindow.WriteOnTextBox("**** BOOTLOADER FLASHING STARTED: ****");
                //Parse android sending the route and arguments, after this, start flashing.
                //Use: Dirpath is the execution place + "\\Component\\yourcomponent_" + c.parseAndroidVersion() + "\\qboot.exe".
                //ParseAndroidVersion will get the android version path like the list explained.
                string ExecPath = this.DirPath + "\\Component\\falcon_" + c.parseAndroidVersion() + "\\qboot.exe";
                //Print Paths
                Flasher.AppWindow.WriteOnTextBox("EXEC: " + ExecPath);
                Flasher.AppWindow.WriteOnTextBox("DIR: " + this.DirPath);
                //Use: RunWithRedirect(String ExecutableFile, String Args )
                i.RunWithRedirect(ExecPath, "blank-flash");
            }
            //Disable ProgresBar Directly
            c.activateProgressBar(false);
        }
    }
}
