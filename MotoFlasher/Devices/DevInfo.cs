﻿using MotoFlasher.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MotoFlasher.Devices
{
    /// <summary>
    /// API documentation for Motoflasher Devices:
    /// This will handle the device operations and you can create them and publish here.
    /// This Will Apply for Windows MotoFlasher, and This is the "base" Class for handle all.
    /// </summary>
    abstract class DevInfo
    {
        /*Hardcoded List of Android Versions (Use if for file save on folder, button enable/disable, etc.)
         * a422: 4.2.2
         * a43 : 4.3
         * a44 : 4.4
         * a442: 4.4.2
         * a444: 4.4.4
         * 'custom' is the Custom Flasher Button on Recently Motoflasher Version.
         * 
         *
         * For files, use this:
         * Program Directory + \Component\ + device_androidversion \
         * Examples:
         *      for a422: \Component\device_442\
         *      for a43 : \Component\device_43\
         *      for a44 : \Component\device_44\
         *      for a442: \Component\device_442\
         *      for a444: \Component\device_444\
         */
        protected string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public virtual void DeviceSelection()
        {
            /* Control the View of Buttons Enabled / Disabled for Moto G selection,
             * And make it invisible or visible.
             * This is Called When the user click 'Moto ?' on Devices List. */

            //Disable the selection of all android versions:

            Flasher.AppWindow.A422.IsEnabled = false;
            Flasher.AppWindow.A43.IsEnabled = true;
            Flasher.AppWindow.A44.IsEnabled = false;
            Flasher.AppWindow.A442.IsEnabled = true;
            Flasher.AppWindow.A444.IsEnabled = true;
            Flasher.AppWindow.Custom.IsEnabled = true;
            /* Disable items
             * If Android Version Above is True, use "Visibility.Visible",
             * If is False, use "Visibility.Collapsed". */
            Flasher.AppWindow.A422.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A43.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A44.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A442.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.A444.Visibility = Visibility.Collapsed;
            Flasher.AppWindow.Custom.Visibility = Visibility.Collapsed;
        }

        public virtual void RunDetection()
        {
            /* Run Device Detection, this will show if the device 
             * is correctly attached to the PC, and working good with drivers. */
            Flasher.AppWindow.WriteOnTextBox("");
            Flasher.AppWindow.WriteOnTextBox("Error: This Device don't have detection service.");
        }
        public virtual void Flash()
        {
            /* Run flash for Moto ?, Select the Correct Android Version, make Some Steps,
             * and let the job to the internal service */
            Flasher.AppWindow.WriteOnTextBox("Device Flash isn't enabled for this device.");
        }
    }
}
