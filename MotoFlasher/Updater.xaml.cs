﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Xml;
using MotoFlasher.Classes;
namespace MotoFlasher
{
    /// <summary>
    /// Interaction logic for Updater.xaml
    /// </summary>
    public partial class Updater
    {
        //Global Variables are Initialized Here.
        private readonly string _dwpath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\UpdaterTempFiles\\";
        private readonly string _dwexecpath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\UpdaterTempFiles\\updateExec.exe";
        private string _dwin = "";
        private readonly string _remoteXml = "http://www.example.com/updates/examplexml.xml";
        private readonly string _myAppName = "MyAppExaple";
        private readonly System.Windows.Forms.NotifyIcon _myNotifyIcon;
        public Updater()
        {
            /*This Constructor Only Works for Updater Designer.
             * And Run The Program to see the design, dont
             * download anything */
            _myNotifyIcon = new System.Windows.Forms.NotifyIcon();
            Show();
            InitializeComponent();
            TitleWindow.Text = "UPDATE DESIGNER";
            ChangeLogTextBox.AppendText("This Is An Example");
            UpdaterProgressBar.IsIndeterminate = true;
        }
        public Updater(string targetXml, string myAppName)
        {
            /* Normal Constructor With XML Update and App Name,
             * this constructor run a TrayBar Notify if Updates are
             * Needed. */
            //Notify Traybar Enable.
            _myNotifyIcon = new System.Windows.Forms.NotifyIcon();
            //In App Resource Icon.
            _myNotifyIcon.Icon = Properties.Resources.Update;
            //TrayIcon Events.
            _myNotifyIcon.MouseDoubleClick += MyNotifyIcon_MouseDoubleClick;
            _myNotifyIcon.BalloonTipClicked += MyNotifyIcon_BaloonClicked;
            //Constructor Initialize the Variables.
            _remoteXml = targetXml;
            _myAppName = myAppName;
            //Runs Temp Clean for olders updates.
            TempClean();
            //Check for Updates.
            if (CheckLastVersion() == false)
            {
                //Run The Program (and tray notification) if are updates.
                WindowState = WindowState.Minimized;
                Show();
                InitializeComponent();
                _myNotifyIcon.Visible = true;
                TitleWindow.Text = "There is a Update for " + _myAppName;
                AlertMethod("Updater Service", "There is a Update for " + _myAppName + Environment.NewLine + "Click for More Info");
                GetVersion();
            }
            else
            {
                //Close App if aren't updates.
                ReceiveKillApp();
            }
        }
        public void GetVersion()
        {
            /* Get Version from remote XML, Changelog and Download Path.
             * And Get Current Version from Local Assembly Version */
            //Get Current Assembly Version
            CurrentVersion.Text = Assembly.GetEntryAssembly().GetName().Version.ToString();
            //Get Last Assembly Version With XML Updater
            try
            {
                //Load Remote XML.
                var xmlFile = new XmlDocument();
                xmlFile.Load(_remoteXml);
                //Get UpdaterEngine Root Tag.
                var updaterRootTag = xmlFile.GetElementsByTagName("UpdaterEngine");
                //Get Motoflasher App Tag
                var appTag = ((XmlElement)updaterRootTag[0]).GetElementsByTagName(_myAppName);
                //Get Windows OS Tag
                var osTag = ((XmlElement)appTag[0]).GetElementsByTagName("Windows");
                //Get Changelog Items (For Text In Updater).
                var changelogDescription = ((XmlElement)osTag[0]).GetElementsByTagName("Description");
                foreach (XmlElement node in osTag)
                {
                    //Get Last Assembly (to Put On Screen Text).
                    var remoteVersion = node.GetElementsByTagName("WinLastAssembly");
                    NewVersion.Text = remoteVersion[0].InnerText;
                    //Get DownloadPath (for download file).
                    var downloadPathFile = node.GetElementsByTagName("WinDownloadPath");
                    _dwin = downloadPathFile[0].InnerText;
                }

                foreach (XmlElement node in changelogDescription)
                {
                    try
                    {
                        //Try Parse Changelog Lines (And Represent it on Screen).
                        var changelogLine = node.GetElementsByTagName("ChangeLogTextLine");
                        for (var i = 0; i < changelogLine.Count; i++)
                        {
                            ChangeLogTextBox.AppendText(changelogLine[i].InnerText + Environment.NewLine);
                            ChangeLogTextBox.ScrollToEnd();
                        }
                    }

                    catch
                    {
                        //Show Error if can get Changelog.
                        ChangeLogTextBox.AppendText("WARNING: NO CHANGELOG OR CHANGELOG ERROR." + Environment.NewLine);
                    }
                }
                try
                {
                    //Try to Get Size of the Download File.
                    var req = (HttpWebRequest)WebRequest.Create(_dwin);
                    req.Method = "HEAD";
                    var resp = (HttpWebResponse)req.GetResponse();
                    var dwSizeOnMb = (resp.ContentLength / 1024f) / 1024f;
                    DownloadSize.Text = dwSizeOnMb.ToString("F1") + " MB";
                }
                catch
                {
                    //Catch Error if can't get the Download Size.
                    AlertMethod("ERROR WITH UPDATER", "Error while trying to check updates");
                    Hide();
                }
            }
            catch
            {
                //Catch Error if can't get the XML File.
                AlertMethod("ERROR WITH UPDATER", "Error while trying to check updates");
                Hide();
            }
        }
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            /*Control Download, Create Temp Dir and Call ProgressBar Event */
            //Create Directory When Downloads will stay.
            if (!Directory.Exists(_dwpath))
            {
                Directory.CreateDirectory(_dwpath);

            }
            //Download Info.
            var client = new WebClient();
            client.DownloadProgressChanged += Updater_DownloadProgressChanged;
            client.DownloadFileCompleted += Updater_DownloadFileCompleted;
            client.DownloadFileAsync(new Uri(_dwin), _dwexecpath);
            //Visual Changes.
            NoThanksBtn.IsEnabled = false;
            UpdateBtn.IsEnabled = false;
        }
        private void Updater_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            /* Event that represent the Change on Progress Bar for the
             * Download, is called with Download Start */
            var bytesIn = double.Parse(e.BytesReceived.ToString());
            var totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            var percentage = bytesIn / totalBytes * 100;
            UpdaterProgressBar.Value = int.Parse(Math.Truncate(percentage).ToString(CultureInfo.InvariantCulture));
        }

        void Updater_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            /* Event that is Called When the Download is Completed,
             * it will Run the Updater and close the app */
            var proc = new Process {StartInfo = {FileName = _dwexecpath}};
            AlertMethod("Download Completed", "The Application will proceed to update");
            proc.Start();
            Thread.Sleep(4000);
            Window_Closing();
            Environment.Exit(0);
        }
        internal bool CheckLastVersion()
        {
            try
            {
                /* This check if you are Running the Latest Version,
                 * it Uses the Local Assembly and Remote Assembly on XML */
                //Initialize Assembly Containers (Remote will need to stay initialized).
                var currentVersion = Assembly.GetEntryAssembly().GetName().Version.ToString();
                var targetVersion = "";
                //Start Loading XML Remote File.
                var xmlFile = new XmlDocument();
                xmlFile.Load(_remoteXml);
                //Get UpdaterEngine Root Tag.
                var updaterRootTag = xmlFile.GetElementsByTagName("UpdaterEngine");
                //Get Motoflasher App Tag
                var appTag = ((XmlElement)updaterRootTag[0]).GetElementsByTagName(_myAppName);
                //Get Windows OS Tag
                var osTag = ((XmlElement)appTag[0]).GetElementsByTagName("Windows");
                foreach (var remoteVersion in from XmlElement node in osTag select node.GetElementsByTagName("WinLastAssembly"))
                {
                    targetVersion = remoteVersion[0].InnerText;
                }
                //Compare Actual Version with Latest Version.
                if (targetVersion != currentVersion)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                //Catch Error if can't fetch Remote XML.
                AlertMethod("Conection Failed", " Error Trying to connect to updates server");
            }
            return true;
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            /* This Event Handle the Minimize to Traybar:
             * If is Minimized, Hide from Taskbar and Show In traybar.
             * If is Normal, Show on TaskBar and Hide from TrayBar.  */
            switch (WindowState)
            {
                case WindowState.Minimized:
                    ShowInTaskbar = false;
                    _myNotifyIcon.Visible = true;
                    break;
                case WindowState.Normal:
                    _myNotifyIcon.Visible = false;
                    ShowInTaskbar = true;
                    break;
            }
        }

        private void AlertMethod(string alertTitle, string alertText)
        {
            /* Choose The Better Alert:
             * If it Minimized, it will show a BaloonTip for 400ms.
             * If it Normal, it will show a Messagebox with Ok Button. */
            switch (WindowState)
            {
                case WindowState.Minimized:
                    _myNotifyIcon.Visible = true;
                    _myNotifyIcon.BalloonTipTitle = alertTitle;
                    _myNotifyIcon.BalloonTipText = alertText;
                    _myNotifyIcon.ShowBalloonTip(400);
                    break;
                case WindowState.Normal:
                    MessageBox.Show(alertText, alertTitle, MessageBoxButton.OK);
                    break;
            }
        }

        private void TempClean()
        {
            /* Clean Temporaly File from Download Temp. */
            if (File.Exists(_dwexecpath))
            {
                File.Delete(_dwexecpath);
            }
        }
        private void NoThanksBtn_Click(object sender, RoutedEventArgs e)
        {
            /* Inmediatly Closes the app */
            ReceiveKillApp();
        }
        private void Window_Closing(Object sender = null ,CancelEventArgs e = null)
        {
            /*Check if you can Exit The app */
            if ((NoThanksBtn.IsEnabled == false) && (e != null))
            {
                e.Cancel = true;
            }
            ReceiveKillApp();
        }
        private void MyNotifyIcon_MouseDoubleClick(object sender,
            System.Windows.Forms.MouseEventArgs e)
        {
            /* Event that Opens this Window:
             * Double Clicking Tray Icon */
            WindowState = WindowState.Normal;
        }
        private void MyNotifyIcon_BaloonClicked(object sender, EventArgs e)
        {
            /* Event that Opens this Window:
             * Clicking BallonTip Notification */
            WindowState = WindowState.Normal;
        }
        public void ReceiveKillApp()
        {
            _myNotifyIcon.Visible = false;
            _myNotifyIcon.Dispose();
            Close();
        }
    }
}
